Numerical wave tank
===================

.. feed-entry::
    :author: Tormod Landet
    :date: 2019-01-08

A short video showing the results from running a numerical wave tank in
Ocellaris and measuring the inline force on a bottom mounted cylinder. The
video has been uploaded to
`YouTube <https://www.youtube.com/watch?v=DZvvZKBaiKc>`_ and is also embedded
below.

..  youtube:: DZvvZKBaiKc
    :width: 80%

The input files, results, and plotting scripts can be found on Zenodo_. A paper
describing the simulation is also available :cite:`landet_3D_wave`.

.. _Zenodo: https://zenodo.org/record/2587038
