New release, Ocellaris version 2019.1.0
================================================

.. feed-entry::
    :author: Tormod Landet
    :date: 2019-05-15

Ocellaris has been updated to support FEniCS version 2019.1.0. A new version of Ocellaris with the same version number has been released. There are no other changes except support for the latest version of PyYAML, 5.1, which is now required to run Ocellaris.

.. figure:: https://www.ocellaris.org/figures/fenics-logo-512.png
    :width: 25%
    :align: center
    :alt: FEniCS logo

See :ref:`label-installing-ocellaris` for information about installing and testing the new version.
